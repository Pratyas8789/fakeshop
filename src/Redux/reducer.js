import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from 'axios'

const initialState = {
    allProduct: "",
    productDetails: "",
    isLoading: false,
    totalItems: 0,
    popup: false,
    productId: "",
    totalPrice: 0,
    quantity: {},
    cartDetails: []
}

export const getProducts = createAsyncThunk("getproduct", async () => {
    return axios.get("https://fakestoreapi.com/products")
        .catch((err) => console.log(err.message))
})

export const getProductDetails = createAsyncThunk("getProductDetails", async (id) => {
    return axios.get(`https://fakestoreapi.com/products/${id}`)
        .catch((err) => console.log(err.message))
})

const productSlice = createSlice({
    name: "products",
    initialState,
    reducers: {
        handlePopup: (state, { payload }) => {
            state.popup = !state.popup
            state.productId = payload
        },
        handleCart: (state, { payload }) => {
            if (state.quantity.hasOwnProperty(payload.id)) {
                state.quantity[payload.id]++
                state.totalItems++
                state.totalPrice += payload.price
            } else {
                state.quantity[payload.id] = 1
                state.totalItems++
                state.cartDetails.push(payload)
                state.totalPrice += payload.price
            }
        },
        handleIncrease: (state, { payload }) => {
            state.quantity[payload.id]++
            state.totalItems++
            state.totalPrice += payload.price
        },
        handleDecrease: (state, { payload }) => {
            state.quantity[payload.id]--
            state.totalItems--
            state.totalPrice -= payload.price
        },
        handleRemoveItems: (state, { payload }) => {
            state.totalItems -= state.quantity[payload.id]
            state.totalPrice -= payload.price * state.quantity[payload.id]
            delete state.quantity[payload.id]
            state.cartDetails = state.cartDetails.filter(eachCartDetails => eachCartDetails.id !== payload.id)
        }
    },
    extraReducers: {
        [getProducts.pending]: (state) => {
            state.isLoading = true
        },
        [getProducts.fulfilled]: (state, { payload }) => {
            state.isLoading = false
            state.allProduct = payload.data
        },
        [getProducts.rejected]: (state) => {
            state.isLoading = false
        },
        [getProductDetails.pending]: (state) => {
            state.isLoading = true
        },
        [getProductDetails.fulfilled]: (state, { payload }) => {
            state.isLoading = false
            state.productDetails = payload.data
        },
        [getProductDetails.rejected]: (state) => {
            state.isLoading = false
        }
    }
})

export const { handlePopup, handleCart, handleIncrease, handleDecrease, handleRemoveItems } = productSlice.actions

export default productSlice.reducer