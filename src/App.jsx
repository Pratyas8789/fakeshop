import { useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from './components/NavBar';
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import HeroSection from './components/HeroSection';
import Cart from './components/Cart';
import CardDetails from './components/CardDetails';
import Error from './components/Error';
import { useDispatch } from 'react-redux';
import { getProducts } from './Redux/reducer';

function App() {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getProducts())
  }, [])

  return (
    <>
      <BrowserRouter>
        <NavBar />
        <Routes>
          <Route exact path='/' element={<HeroSection />} />
          <Route exact path='/cart' element={<Cart />} />
          <Route exact path='/details/:id' element={<CardDetails />} />
          <Route exact path='/*' element={<Error />} />
        </Routes>
      </BrowserRouter>
    </>
  )
}

export default App