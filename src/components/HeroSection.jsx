import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { v4 as uuidv4 } from 'uuid'
import { Link } from 'react-router-dom';
import { handleCart, handlePopup } from '../Redux/reducer';
import Spinner from 'react-bootstrap/esm/Spinner';

export default function HeroSection() {
    const { allProduct, popup, productId, isLoading, cartDetails } = useSelector(store => store.products)
    const dispatch = useDispatch()

    const handleClick = (eachProduct) => {
        dispatch(handlePopup(eachProduct.id))
        dispatch(handleCart(eachProduct))
        setTimeout(() => {
            dispatch(handlePopup())
        }, 1000);
    }

    let displayProducts
    if (Array.isArray(allProduct) && allProduct.length > 0) {
        displayProducts = allProduct.map((eachProduct) => {
            return (
                <Card key={uuidv4()} className='m-3 p-3 align-items-center' style={{ width: '19rem', height: "40rem" }}>
                    <div className='h-50 w-100'>
                        <Link to={`/details/${eachProduct.id}`}>
                            <Card.Img className='h-100 w-100' variant="top" src={eachProduct.image} />
                        </Link>
                    </div>
                    <Card.Body className='h-50 w-100 p-0'  >
                        <Card.Title className='h-50 m-0 text-center pt-2'  >{eachProduct.title}</Card.Title>
                        <Card.Text className='h-25 m-0 text-center'>
                            Category : {eachProduct.category} <br />
                            <h5 className='text-warning' >${eachProduct.price}</h5>
                        </Card.Text>
                        <div className='d-flex justify-content-between ps-2 pe-2 align-items-center h-25' >
                            <Button variant="warning" onClick={() => handleClick(eachProduct)} >Add to cart</Button>
                            {popup && (productId === eachProduct.id) && <Button variant="white">Added In Cart</Button>}
                        </div>
                    </Card.Body>
                </Card>
            )
        })
    }

    return (
        <div className='d-flex flex-wrap m-5 justify-content-around' >
            {isLoading ? <h1>Please wait <Spinner /></h1> : displayProducts}
        </div>
    )
}