import React from 'react'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useDispatch, useSelector } from 'react-redux';
import { handleDecrease, handleIncrease, handleRemoveItems } from '../Redux/reducer';
import { v4 as uuidv4 } from 'uuid'

export default function Cart() {
  const { quantity, cartDetails, totalPrice } = useSelector(store => store.products)

  const dispatch = useDispatch()
  let displayCartItems

  if (Array.isArray(cartDetails) && cartDetails.length > 0) {
    displayCartItems = cartDetails.map((eachcartDetails) => {
      return (
        <Card key={uuidv4()} className='cartContainer d-flex flex-row  shadow-lg rounded-5 m-2' style={{ height: "18rem" }}>
          <Card.Img className='w-25 h-100 p-3' variant="top" src={eachcartDetails.image} />
          <Card.Body >
            <Card.Title >{eachcartDetails.title}</Card.Title>
            <Card.Text className='d-flex flex-row justify-content-between align-items-center' >
              <h5>Quantity: {quantity[eachcartDetails.id]}</h5>
              <div >
                {(quantity[eachcartDetails.id] > 1) ? <> <Button variant="secondary" onClick={() => dispatch(handleDecrease(eachcartDetails))} >-</Button>{' '} </> : <><Button variant="secondary" onClick={() => dispatch(handleDecrease(eachcartDetails))} disabled >-</Button>{' '} </>}
                <Button variant="secondary" onClick={() => dispatch(handleIncrease(eachcartDetails))} >+</Button>{' '}
              </div>
            </Card.Text>
            <Card.Text className='d-flex flex-row justify-content-between text-warning' >
              <h4>Price: $ {(eachcartDetails.price * quantity[eachcartDetails.id]).toFixed(2)}</h4>
            </Card.Text>
            <Button className='rounded-5' variant="danger" onClick={() => dispatch(handleRemoveItems(eachcartDetails))} >remove item</Button>
          </Card.Body>
        </Card>
      )
    })
  }

  return (
    <>
      {Object.keys(quantity).length > 0 ?
        <div style={{ height: "91vh" }} >
          <div className='h-75 d-flex flex-column align-items-center overflow-scroll' >
            {displayCartItems}
          </div>
          <div className='h-25 d-flex justify-content-center '>
            <div className='p-3 m-4 shadow-lg rounded-5' style={{ width: '48rem' }} >
              <div className='h-50 w-100 d-flex justify-content-between align-items-center' >
                <h3> SUBTOTAL</h3>
                <h3 className='text-warning' >${totalPrice.toFixed(2)} </h3>
              </div>
              <div className='h-50 w-100 d-flex align-items-center' >
                <Button className='w-100 ' onClick={() => alert(`Checkout - Subtotal: $ ${totalPrice.toFixed(2)}`)} variant="dark">CHECKOUT</Button>
              </div>
            </div>
          </div>
        </div>
        : <h1>Cart is empty, please add items!!</h1>}
    </>
  )
}