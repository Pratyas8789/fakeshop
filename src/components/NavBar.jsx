import React from 'react'
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import { BsFillCartFill } from 'react-icons/bs';
import { AiFillHome } from 'react-icons/ai';
import { useSelector } from 'react-redux';

export default function NavBar() {
    const { totalItems } = useSelector(store => store.products)
    return (
        <Navbar className='p-0' bg="light" variant="light">
            <Container className='w-100 p-0'>
                <Navbar.Brand > <img alt="" srcSet="https://react-crud-using-redux.vercel.app/static/media/logo.842cca0db7b3c0328540.png" width={"150px"} /> </Navbar.Brand>
                <Navbar.Brand className='p-0 m-0'  >
                    <Link to='/' style={{ textDecoration: "none" }}>
                        <Button variant="light"><AiFillHome size={30} /><br /><h6>Home</h6></Button>{' '}
                    </Link>
                    <Link to='cart'>
                        <Button variant="light" className='p-0' >
                            <div className='d-flex w-100 h-75 ' >
                                <BsFillCartFill size={30} />
                                <button className='bg-warning rounded-circle border-0 m-1' style={{ width: "25px" }} >{totalItems}</button>
                            </div>
                            <h6>Cart</h6>
                        </Button>{' '}
                    </Link>
                </Navbar.Brand>
            </Container>
        </Navbar >
    )
}