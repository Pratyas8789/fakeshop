import React from 'react'
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';

export default function () {
    return (
        <div className='d-flex flex-column justify-content-center align-items-center ' >
            <img alt="" srcSet="https://img.freepik.com/free-vector/404-error-with-tired-person-concept-illustration_114360-7879.jpg?t=st=1684915965~exp=1684916565~hmac=da240731c942ae532829c01c4211509604c565b7a3287becd5d790490c508757" />
            <Link to='/' >
                <Button variant="primary">Go to home page</Button>{' '}
            </Link>
        </div>
    )
}