import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getProductDetails, handleCart, handlePopup } from '../Redux/reducer'
import { useParams } from 'react-router-dom'
import Button from 'react-bootstrap/Button';
import { BsStarFill } from 'react-icons/bs';
import { BsFillPersonFill } from 'react-icons/bs';
import { BiCategory } from 'react-icons/bi';
import Spinners from './Spinner';

export default function CardDetails() {
  const { productDetails, isLoading, popup, productId, cartDetails } = useSelector(store => store.products)
  const parms = useParams()
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getProductDetails(parms.id))
  }, [])

  const handleClick = (productDetails) => {
    dispatch(handlePopup(productDetails.id))
    dispatch(handleCart(productDetails))
    setTimeout(() => {
      dispatch(handlePopup())
    }, 500);
  }
  return (
    <>
      {isLoading ? <h1>Please wait<Spinners /></h1> : productDetails !== "" &&
        <div className='mainContainer d-flex justify-content-center align-items-center' >
          <div className='cartDetailsContainer  shadow-lg rounded-5' >
            <div className='cartDetailsContainerImg h-100 ' >
              <div className='p-4' style={{ height: "85%" }} > <img alt="" srcSet={productDetails.image} width={"100%"} height={"100%"} /> </div>
              <div className='d-flex justify-content-center align-items-center' style={{ height: "15%" }} > <h5 className='text-warning' >Price : {productDetails.price}</h5> </div>
            </div>
            <div className='cartDetailsContainerDetails h-100 ' >
              <div className='m-4' >
                <h4>{productDetails.title}</h4>
              </div>
              <div className='ms-4' >
                <h6><BiCategory size={20} /> : {productDetails.category}</h6>
              </div>
              <div className='m-4 d-flex'>
                <p className='me-4'><BsStarFill className='text-warning' size={20} />{productDetails.rating.rate}</p>
                <p><BsFillPersonFill size={20} />{productDetails.rating.count}</p>
              </div>
              <div className='m-4' >
                <h6>{productDetails.description}</h6>
              </div>
              <div className='m-4' >
                <Button variant="warning" onClick={() => handleClick(productDetails)}>Add to cart</Button>{' '}
                {popup && (productId === productDetails.id) && <Button variant="white">Added In Cart</Button>}
              </div>
            </div>
          </div>
        </div>
      }</>
  )
}